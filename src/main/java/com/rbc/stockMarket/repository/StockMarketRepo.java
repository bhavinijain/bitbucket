package com.rbc.stockMarket.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.rbc.stockMarket.dataobject.StockMarket;
import java.lang.String;
import java.util.List;

public interface StockMarketRepo extends JpaRepository<StockMarket, Long>{
	
	List<StockMarket> findByStock(String stock);
}
