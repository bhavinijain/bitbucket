package com.rbc.stockMarket.controller;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.rbc.stockMarket.dataobject.StockMarket;
import com.rbc.stockMarket.service.StockService;

@RestController
public class StockController {
	
	@Autowired
	StockService stockService;

	@RequestMapping(value="/bulkFileUpload", method=RequestMethod.POST)
	public void bulkFileUploadStock(@RequestParam("file") MultipartFile file) throws IOException, ParseException
	{
		stockService.bulkUpload(file);
	}

	@RequestMapping(value="/getStock/{stock}", method=RequestMethod.GET)
	public List<StockMarket> getStock(@PathVariable String stock){
		
		List<StockMarket> markets = stockService.findByStock(stock);
		return markets;
		
	}
	
	@RequestMapping(value="/addStock", method=RequestMethod.POST)
	public StockMarket addStock(@RequestBody StockMarket stock){
		
		return stockService.addMarketRecord(stock);
	}
	
	@RequestMapping(value="/bulkDataUpload", method=RequestMethod.POST)
	public List<StockMarket> bulkUploadStock(@RequestParam("file") List<StockMarket> stockMarkets)
	{
		return stockService.addMarketRecords(stockMarkets);
	}

}
