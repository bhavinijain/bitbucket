package com.rbc.stockMarket.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.rbc.stockMarket.dataobject.StockMarket;
import com.rbc.stockMarket.helper.FileHelper;
import com.rbc.stockMarket.repository.StockMarketRepo;

@Service
public class StockService {
	
	@Autowired
	FileHelper fileHelper;
	
	@Autowired
	StockMarketRepo stockMarketRepo;
	
	public List<StockMarket> bulkUpload(MultipartFile file) throws IOException, ParseException
	{
		List<StockMarket> markets = fileHelper.parseFile(file.getInputStream());
		
		markets = addMarketRecords(markets);
		
		return markets;
	}

	public List<StockMarket> addMarketRecords(List<StockMarket> markets) {
		return stockMarketRepo.saveAll(markets);
	}

	public List<StockMarket> findByStock(String stock) {
		
		List<StockMarket> markets = stockMarketRepo.findByStock(stock);
		return markets;
	}

	public StockMarket addMarketRecord(StockMarket stock) {
		return stockMarketRepo.save(stock);
		
	}

}
