package com.rbc.stockMarket.helper;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Component;

import com.rbc.stockMarket.dataobject.StockMarket;

@Component
public class FileHelper {

	public List<StockMarket> parseFile(InputStream is) throws IOException, ParseException 
	{
		List<StockMarket> list = new ArrayList<StockMarket>();
		Reader csvData = new BufferedReader(new InputStreamReader(is));
		CSVParser parser = CSVParser.parse(csvData, CSVFormat.DEFAULT.withFirstRecordAsHeader());	 
		for(CSVRecord fileRecord : parser )
		{
			list.add(StockMarket.builder().setFileRecord(fileRecord).build());
		}
		return list;
	}

}
