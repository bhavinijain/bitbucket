package com.rbc.stockMarket.dataobject;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.csv.CSVRecord;

@Entity
public class StockMarket {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private int quarter;
	private String stock;
	private Date date;
	private double open;
	private double high;
	private double low;
	private double close;
	private double volume;
	private double percentChangePrice;
	private double percentVolumeChangeOverAWeek;
	private double previousWeekVolume;
	private double nextWeekOpen;
	private double nextWeekClose;
	private double percentChangeNextWeek;
	private double daysToDividend;
	private double percentRecentNextDividend;

	
	
	@Override
	public String toString() {
		return "StockMarket [id=" + id + ", quarter=" + quarter + ", stock=" + stock + ", date=" + date + ", open="
				+ open + ", high=" + high + ", low=" + low + ", close=" + close + ", volume=" + volume
				+ ", percentChangePrice=" + percentChangePrice + ", percentVolumeChangeOverAWeek="
				+ percentVolumeChangeOverAWeek + ", previousWeekVolume=" + previousWeekVolume + ", nextWeekOpen="
				+ nextWeekOpen + ", nextWeekClose=" + nextWeekClose + ", percentChangeNextWeek=" + percentChangeNextWeek
				+ ", daysToDividend=" + daysToDividend + ", percentRecentNextDividend=" + percentRecentNextDividend
				+ "]";
	}

	//private StockMarket(){}

	public static Builder builder()
	{
		return new Builder();
	}

	public static class Builder
	{
		private CSVRecord fileRecord;
		private SimpleDateFormat format=new SimpleDateFormat("mm/dd/yyyy");

		public Builder setFileRecord(CSVRecord fileRecord)
		{
			this.fileRecord=fileRecord;
			return this;
		}

		public StockMarket build() throws ParseException
		{
			StockMarket s = new StockMarket();
			
			s.quarter=Integer.parseInt(fileRecord.get("quarter"));
			s.stock=fileRecord.get("stock");
			s.date=format.parse(fileRecord.get("date"));
			s.open=getDoubleValueForDollar(fileRecord.get("open"));
			s.high=getDoubleValueForDollar(fileRecord.get("high"));
			s.low=getDoubleValueForDollar(fileRecord.get("low"));
			s.close=getDoubleValueForDollar(fileRecord.get("close"));
			s.volume=getDoubleValue(fileRecord.get("volume"));
			s.percentChangePrice=getDoubleValue(fileRecord.get("percent_change_price"));
			s.percentVolumeChangeOverAWeek=getDoubleValue(fileRecord.get("percent_change_volume_over_last_wk"));
			s.previousWeekVolume=getDoubleValue(fileRecord.get("previous_weeks_volume"));
			s.nextWeekOpen=getDoubleValueForDollar(fileRecord.get("next_weeks_open"));
			s.nextWeekClose=getDoubleValueForDollar(fileRecord.get("next_weeks_close"));
			s.percentChangeNextWeek=getDoubleValue(fileRecord.get("percent_change_next_weeks_price"));
			s.daysToDividend=getDoubleValue(fileRecord.get("days_to_next_dividend"));
			s.percentRecentNextDividend=getDoubleValue(fileRecord.get("percent_return_next_dividend"));
			System.out.println(s);
			
			return s;
		}

		private static double getDoubleValueForDollar(String dollarValue)
		{
			if(dollarValue!=null)
			{	
				return Double.parseDouble(dollarValue.substring(1, dollarValue.length()));
			}
			return 0.0d;
		}
		
		private static double getDoubleValue(String dollarValue)
		{
			if(dollarValue!=null && !dollarValue.equals(""))
			{	
				return Double.parseDouble(dollarValue);
			}
			return 0.0d;
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getQuarter() {
		return quarter;
	}

	public void setQuarter(int quarter) {
		this.quarter = quarter;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getOpen() {
		return open;
	}

	public void setOpen(double open) {
		this.open = open;
	}

	public double getHigh() {
		return high;
	}

	public void setHigh(double high) {
		this.high = high;
	}

	public double getLow() {
		return low;
	}

	public void setLow(double low) {
		this.low = low;
	}

	public double getClose() {
		return close;
	}

	public void setClose(double close) {
		this.close = close;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getPercentChangePrice() {
		return percentChangePrice;
	}

	public void setPercentChangePrice(double percentChangePrice) {
		this.percentChangePrice = percentChangePrice;
	}

	public double getPercentVolumeChangeOverAWeek() {
		return percentVolumeChangeOverAWeek;
	}

	public void setPercentVolumeChangeOverAWeek(double percentVolumeChangeOverAWeek) {
		this.percentVolumeChangeOverAWeek = percentVolumeChangeOverAWeek;
	}

	public double getPreviousWeekVolume() {
		return previousWeekVolume;
	}

	public void setPreviousWeekVolume(double previousWeekVolume) {
		this.previousWeekVolume = previousWeekVolume;
	}

	public double getNextWeekOpen() {
		return nextWeekOpen;
	}

	public void setNextWeekOpen(double nextWeekOpen) {
		this.nextWeekOpen = nextWeekOpen;
	}

	public double getNextWeekClose() {
		return nextWeekClose;
	}

	public void setNextWeekClose(double nextWeekClose) {
		this.nextWeekClose = nextWeekClose;
	}

	public double getPercentChangeNextWeek() {
		return percentChangeNextWeek;
	}

	public void setPercentChangeNextWeek(double percentChangeNextWeek) {
		this.percentChangeNextWeek = percentChangeNextWeek;
	}

	public double getDaysToDividend() {
		return daysToDividend;
	}

	public void setDaysToDividend(double daysToDividend) {
		this.daysToDividend = daysToDividend;
	}

	public double getPercentRecentNextDividend() {
		return percentRecentNextDividend;
	}

	public void setPercentRecentNextDividend(double percentRecentNextDividend) {
		this.percentRecentNextDividend = percentRecentNextDividend;
	}
	
	
}
