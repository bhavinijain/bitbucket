package com.rbc.stockMarket;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;

import com.rbc.stockMarket.dataobject.StockMarket;
import com.rbc.stockMarket.helper.FileHelper;
import com.rbc.stockMarket.repository.StockMarketRepo;
import com.rbc.stockMarket.service.StockService;

@SpringBootTest
public class StockServiceTest {

	@MockBean
	private StockMarketRepo stockMarketRepo;
	
	@MockBean
	private FileHelper fileHelper;
	
	@Autowired
	private StockService stockService;
	
	@Test
	public void testUploadBulkData() throws Exception{
		String data = "quarter,stock,date,open,high,low,close,volume,"
				+ "percent_change_price,percent_change_volume_over_last_wk,previous_weeks_volume,next_weeks_open,"
				+ "next_weeks_close,percent_change_next_weeks_price,days_to_next_dividend,percent_return_next_dividend\n"
				+ "1,AA,,,,,,,,,,,,,,";
		MockMultipartFile file1 = new MockMultipartFile("singleFile", "dow_jones_index.data",
				null, data.getBytes());
		
		
		List<StockMarket> markets = new ArrayList<StockMarket>();
		
		StockMarket stockMarket = new StockMarket();
		stockMarket.setQuarter(1);
		stockMarket.setStock("AA");
		markets.add(stockMarket);
		
		doReturn(markets).when(fileHelper).parseFile(any(InputStream.class));
		
		doReturn(markets).when(stockService).addMarketRecords((List<StockMarket>) any());
		
		List<StockMarket> marketList = stockService.bulkUpload(file1);
		
		Assertions.assertNotNull(marketList);
		Assertions.assertNotNull(marketList.get(0));
		Assertions.assertEquals("AA", marketList.get(0).getStock());
	}
	
	@Test
	public void testAddMarketRecord() throws Exception{
		
		StockMarket stockMarket = new StockMarket();
		stockMarket.setQuarter(1);
		stockMarket.setStock("AA");
		doReturn(stockMarket).when(stockMarketRepo).save(any(StockMarket.class));
		
		StockMarket market = stockService.addMarketRecord(stockMarket);
		
		Assertions.assertNotNull(market);
		Assertions.assertEquals("AA", market.getStock());
		
	}
	
	@Test
	public void testAddMarketRecords() throws Exception{
		
		List<StockMarket> markets = new ArrayList<StockMarket>();
				
		StockMarket stockMarket = new StockMarket();
		stockMarket.setQuarter(1);
		stockMarket.setStock("AA");
		markets.add(stockMarket);
		doReturn(markets).when(stockMarketRepo).saveAll((Iterable<StockMarket>) any());
		
		List<StockMarket> marketList = stockService.addMarketRecords(markets);
		
		Assertions.assertNotNull(marketList);
		Assertions.assertNotNull(marketList.get(0));
		Assertions.assertEquals("AA", marketList.get(0).getStock());
		
	}
	
	@Test
	public void testFindByStockNotFound() throws Exception{
		
		doReturn(Optional.empty()).when(stockMarketRepo).findByStock("AA");
		
		List<StockMarket> marketList = stockService.findByStock("AA");
		
		Assertions.assertNull(marketList);
	}
	
	@Test
	public void testFindByStock() throws Exception{
	
		List<StockMarket> markets = new ArrayList<StockMarket>();
		
		StockMarket stockMarket = new StockMarket();
		stockMarket.setQuarter(1);
		stockMarket.setStock("AA");
		markets.add(stockMarket);
		
		doReturn(markets).when(stockMarketRepo).findByStock("AA");
		
		List<StockMarket> marketList = stockService.findByStock("AA");
		
		Assertions.assertTrue(marketList.size()>0);
		Assertions.assertSame(marketList.get(0), markets.get(0));
	}
	
	

}
