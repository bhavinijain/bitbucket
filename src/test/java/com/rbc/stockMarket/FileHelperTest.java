package com.rbc.stockMarket;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;

import com.rbc.stockMarket.dataobject.StockMarket;
import com.rbc.stockMarket.helper.FileHelper;

@SpringBootTest
public class FileHelperTest {
	
	@Autowired
	private FileHelper fileHelper;
	
	@Test
	public void testParseFile() throws Exception{
		String data = "quarter,stock,date,open,high,low,close,volume,"
				+ "percent_change_price,percent_change_volume_over_last_wk,previous_weeks_volume,next_weeks_open,"
				+ "next_weeks_close,percent_change_next_weeks_price,days_to_next_dividend,percent_return_next_dividend\n"
				+ "1,AA,,,,,,,,,,,,,,";
		MockMultipartFile file1 = new MockMultipartFile("singleFile", "dow_jones_index.data",
				null, data.getBytes());
		
		
		List<StockMarket> markets = new ArrayList<StockMarket>();
		
		StockMarket stockMarket = new StockMarket();
		stockMarket.setQuarter(1);
		stockMarket.setStock("AA");
		markets.add(stockMarket);
		
		List<StockMarket> marketList = fileHelper.parseFile(file1.getInputStream());
		
		Assertions.assertNotNull(marketList);
		Assertions.assertNotNull(marketList.get(0));
		Assertions.assertEquals("AA", marketList.get(0).getStock());
	}
	
	public void testParseFileEmpty() throws Exception{
		String data = "quarter,stock,date,open,high,low,close,volume,"
				+ "percent_change_price,percent_change_volume_over_last_wk,previous_weeks_volume,next_weeks_open,"
				+ "next_weeks_close,percent_change_next_weeks_price,days_to_next_dividend,percent_return_next_dividend";
		
		MockMultipartFile file1 = new MockMultipartFile("singleFile", "dow_jones_index.data",
				null, data.getBytes());
		
		List<StockMarket> marketList = fileHelper.parseFile(file1.getInputStream());
		
		Assertions.assertTrue(marketList.isEmpty());
	}

}
