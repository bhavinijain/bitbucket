package com.rbc.stockMarket;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.rbc.stockMarket.controller.StockController;
import com.rbc.stockMarket.dataobject.StockMarket;
import com.rbc.stockMarket.service.StockService;

@SpringBootTest
public class StockControllerTest {
	
	@MockBean
	private StockService stockService;
	
	@Autowired
	private StockController stockController;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testUploadBulkFile() throws Exception{
		String data = "quarter,stock,date,open,high,low,close,volume,"
				+ "percent_change_price,percent_change_volume_over_last_wk,previous_weeks_volume,next_weeks_open,"
				+ "next_weeks_close,percent_change_next_weeks_price,days_to_next_dividend,percent_return_next_dividend\n"
				+ "1,AA,,,,,,,,,,,,,,";
		MockMultipartFile file1 = new MockMultipartFile("singleFile", "dow_jones_index.data",
				null, data.getBytes());
		
		
		mockMvc.perform(MockMvcRequestBuilders.multipart("/bulkFileUpload").file(file1))
		.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void testAddMarketRecord() throws Exception{
		
		StockMarket stockMarket = new StockMarket();
		stockMarket.setQuarter(1);
		stockMarket.setStock("AA");
		doReturn(stockMarket).when(stockService).addMarketRecord(any(StockMarket.class));
		
		StockMarket market = stockController.addStock(stockMarket);
		
		Assertions.assertNotNull(market);
		Assertions.assertEquals("AA", market.getStock());
		
	}
	
	@Test
	public void testFindByStockNotFound() throws Exception{
		
		doReturn(Optional.empty()).when(stockService).findByStock("AA");
		
		List<StockMarket> marketList = stockController.getStock("AA");
		
		Assertions.assertNull(marketList);
	}
	
	@Test
	public void testFindByStock() throws Exception{
	
		List<StockMarket> markets = new ArrayList<StockMarket>();
		
		StockMarket stockMarket = new StockMarket();
		stockMarket.setQuarter(1);
		stockMarket.setStock("AA");
		markets.add(stockMarket);
		
		doReturn(markets).when(stockService).findByStock("AA");
		
		List<StockMarket> marketList = stockController.getStock("AA");
		
		Assertions.assertTrue(marketList.size()>0);
		Assertions.assertSame(marketList.get(0), markets.get(0));
	}
	
	@Test
	public void testUploadBulkData() throws Exception{
	
		List<StockMarket> markets = new ArrayList<StockMarket>();
		
		StockMarket stockMarket = new StockMarket();
		stockMarket.setQuarter(1);
		stockMarket.setStock("AA");
		markets.add(stockMarket);
		
		doReturn(markets).when(stockService).addMarketRecords((List<StockMarket>) any());
		
		List<StockMarket> marketList = stockController.bulkUploadStock(markets);
		
		Assertions.assertTrue(marketList.size()>0);
		Assertions.assertSame(marketList.get(0), markets.get(0));
	}
	
	

}
